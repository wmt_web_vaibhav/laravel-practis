<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detail extends Model
{
    protected $guarded = [

    ];
    public function Employee(){
        return $this->belongsTo(Employee::class,'employee_id','id');
    }
}
