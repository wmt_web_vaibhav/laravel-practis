<?php

namespace App\Http\Controllers;
use App\Comment;
use App\Post;
use App\Author;
use App\Book;
use App\User;
use Illuminate\Http\Request;

class RelationController extends Controller
{

    //one to many

//    public function relation()
//    {
//    $post=Post::find(3);
//    $comment=$post->Comment;
//    echo $comment;
//    }

//    public function relation()
//    {
//        $comment=Comment::find(1);
//        $post=$comment->Post;
//       echo $post;

        //The save method

//        $comment = new Comment(['comment' => 'hello vaibhav.']);
//        $post =Post::find(1);
//       $post->comment()->save($comment);


//        $post->comment()->saveMany([
//            new Comment(['comment' => 'A new comment.']),
//            new Comment(['comment' => 'Another comment.']),
//        ]);

        //The create method

//        $post = Post::find(1);
//        $post->comment()->create(['comment' => 'hello sanket gfdja.']);

//        $post = Post::find(1);
//        $comment=new Comment(1);
//        $comment->post()->associate($post);
//        $comment->save();

//    }

    //many to many

    public function relation()
    {
        $author=Author::find(2);
        $book=Book::find(1);

        $author->books()->sync([1,2,3]);
    echo "<ul>";
        foreach (Author::all() as $author){
            echo "<li>".$author->a_name."<ul><br>";
            if($author->books){
                foreach($author->books as $a){
                    echo "<li>".$a->b_name."</li><br>";
                }
            }
            echo "</ul>";
        }
        echo "</li></ul>";
    }

}
