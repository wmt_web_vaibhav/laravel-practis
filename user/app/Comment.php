<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
Relation::morphMap([
    'image'=>'App\Post',
    'video'=>'App\Post',
]);
class Comment extends Model
{
    public $timestamps = false;
    protected $guarded=[];
    public function commentable(){
        return $this->morphTo();
    }
}
