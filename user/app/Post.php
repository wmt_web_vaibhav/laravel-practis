<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public $timestamps = false;

    public function comments(){
        return $this->morphMany(Comment::class,'commentable');
    }

    public function peoples()
    {
        return $this->belongsToMany('App\People','posts','people_id','id');
    }
    public function countries()
    {
        return $this->belongsToMany('App\Countries','posts','id','id');
    }

    //mutator

    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = strtolower($value);
    }

}
