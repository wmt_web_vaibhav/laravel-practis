<?php

namespace App\Http\Middleware;

use Closure;

class test
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $abc=$request->ip();
        if($abc=='127.0.0.1'){
            return redirect('/home/pwc');
        }
        return $next($request);
    }
}
