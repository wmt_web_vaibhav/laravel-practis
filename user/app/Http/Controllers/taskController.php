<?php

namespace App\Http\Controllers;
use App\Countries;
use App\Comment;
use App\People;
use App\Post;
use Illuminate\Http\Request;

class taskController extends Controller
{
    //Country wise post

    public function cwp()
    {

//        return Post::with(['peoples','comments'])->find(1);

        return Countries::with(['posts'])->get();

    }
    //post wise comment

    public function pwc()
    {
        return Post::with(['comments'])->find(1);
    }

    //update profile image

    public function update_profile(){
        $upd = People::find(1);
        $upd->profile_img = 'new image ';
        $upd->save();
    }

    //add post

    public function store()
    {
        $add =new Post;
        $add->title='Post title';//string
        $add->url='new image';//string
        $add->people_id='1';//int
        $add->postable_type='image';//string image or video
        $add->postable_id='1';//int
        $add->save();
    }

    //comment in any post

    public function comment(){
        $cmt = Post::find(1);

        $cmt->comments()->createMany([
            [
                'comment' => 'new comment.',
            ]
        ]);
    }

    //Accessor

    public function accessor()
    {
        $user = People::find(1);
        $firstName = $user->name;
        echo $firstName;
    }

    //Mutator

    public function mutator()
    {
        $add =new Post;
        $add->setAttribute('title', 'PPPPst title');//string
        $add->url='new image';//string
        $add->people_id='1';//int
        $add->postable_type='image';//string image or video
        $add->postable_id='1';//int
        $add->save();
    }

    public function test(Request $request)
    {

        echo "<form method=''>";
        echo "<input type=\"hidden\" name=\"_token\" value=\"{{ csrf_token() }}\">";
        echo "<input type='text' name='name'>";
        echo "<input type='submit'>";
        echo "</form>";
        $name = $request->input('name');
        return $name;
    }

}
