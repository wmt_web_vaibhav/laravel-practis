<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class People extends Model
{

    //Accessor

    public function getNameAttribute($value)
    {
        return ucfirst($value);

    }
}
