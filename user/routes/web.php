<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home/cwp','taskController@cwp')->middleware('test');
Route::get('/home/pwc','taskController@pwc');
Route::get('/home/updprofile','taskController@update_profile');
Route::get('/home/add','taskController@store');
Route::get('/home/cmt','taskController@comment');
Route::get('/home/accessor','taskController@accessor');
Route::get('/home/mutator','taskController@mutator');
Route::get('/home/test','taskController@test');
Route::get('/home/blade','bladeController@blade');
